import {act, fireEvent, render, screen} from '@testing-library/react-native';
import fetch from 'jest-fetch-mock';
import React from 'react';
import Login from './Login';

describe('Login screen', () => {
  it('has two Login elements', () => {
    const {getAllByText} = render(<Login />);

    expect(getAllByText('Login').length).toBe(2);
  });

  it('displays correct placeholders', () => {
    const {getByPlaceholderText} = render(<Login />);

    getByPlaceholderText('Username');
    getByPlaceholderText('Password');
  });

  it('shows invalid messages when no values in inputs', async () => {
    const {findByTestId, getByTestId} = render(<Login />);
    fireEvent.press(await findByTestId('login-btn'));

    expect(getByTestId('username-error').props.children).toMatch(
      'username invalid',
    );
    expect(getByTestId('password-error').props.children).toMatch(
      'password invalid',
    );
  });

  it('show error messages when incorrent values', async () => {
    render(<Login />);

    fireEvent.changeText(await screen.findByTestId('username-input'), 'aaa');
    fireEvent.changeText(await screen.findByTestId('password-input'), 'bbb');
    fireEvent.press(await screen.findByTestId('login-btn'));

    expect(screen.getByText('username invalid')).toBeDefined;
    expect(screen.queryAllByText('password invalid').length).toBe(1);
  });

  it('do not show error messages when values correct', async () => {
    render(<Login />);

    fireEvent.changeText(await screen.findByTestId('username-input'), 'Mirek');
    fireEvent.changeText(await screen.findByTestId('password-input'), '123');
    fireEvent.press(await screen.findByTestId('login-btn'));

    expect(screen.queryAllByText('username invalid').length).toBe(0);
    expect(screen.queryAllByText('password invalid').length).toBe(0);
  });

  it('show error messages when incorrent values', async () => {
    render(<Login />);

    fireEvent.changeText(await screen.findByTestId('username-input'), 'aaa');
    fireEvent.changeText(await screen.findByTestId('password-input'), 'bbb');
    fireEvent.press(await screen.findByTestId('login-btn'));

    expect(screen.getByText('username invalid')).toBeDefined;
    expect(screen.queryAllByText('password invalid').length).toBe(1);
  });

  it('should send correct values to api', async () => {
    render(<Login />);

    fetch.mockResponseOnce(JSON.stringify({passes: true}));

    fireEvent.changeText(await screen.findByTestId('username-input'), 'Mirek');
    fireEvent.changeText(await screen.findByTestId('password-input'), '123');
    fireEvent.press(await screen.findByTestId('login-btn'));

    await act(() => new Promise(resolve => setImmediate(resolve)));

    expect(fetch.mock.calls).toMatchSnapshot();
  });
});
