import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

const Login = () => {
  const [userName, setUserName] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [userNameValid, setUserNameValid] = useState<boolean>(true);
  const [passwordValid, setPasswordValid] = useState<boolean>(true);

  const onLoginPress = () => {
    let usernameValid = false;
    let passwordValid = false;

    if (userName === 'Mirek') {
      usernameValid = true;
    }

    if (password === '123') {
      passwordValid = true;
    }

    if (usernameValid && passwordValid) {
      fetchData();
    }

    setUserNameValid(usernameValid);
    setPasswordValid(passwordValid);
  };

  const fetchData = async () => {
    await fetch('https://jsonplaceholder.typicode.com/users', {
      method: 'POST',
      body: JSON.stringify({userName, password}),
    })
      .then(response => response.json())
      .then(() => {})
      .catch(error => {
        console.log('error', error);
      });
  };

  return (
    <SafeAreaView style={styles.saveArea}>
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>Login</Text>
        </View>
        <View style={styles.inputWrapper}>
          <TextInput
            testID="username-input"
            style={styles.input}
            placeholder="Username"
            value={userName}
            onChangeText={setUserName}
          />
          {!userNameValid && (
            <Text testID="username-error" style={styles.errorMessage}>
              username invalid
            </Text>
          )}
        </View>

        <View style={styles.inputWrapper}>
          <TextInput
            testID="password-input"
            style={styles.input}
            placeholder="Password"
            value={password}
            onChangeText={setPassword}
          />
          {!passwordValid && (
            <Text testID="password-error" style={styles.errorMessage}>
              password invalid
            </Text>
          )}
        </View>
        <TouchableOpacity
          testID="login-btn"
          style={styles.loginBtn}
          onPress={onLoginPress}>
          <Text style={styles.loginBtnTitle}>Login</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  saveArea: {
    flex: 1,
    backgroundColor: '#fcfafa',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#fcfafa',
    paddingHorizontal: 20,
    paddingVertical: 50,
  },
  input: {
    backgroundColor: '#f0f0f0',
    borderRadius: 10,
    width: '100%',
    height: 50,
    marginTop: 10,
    padding: 10,
    justifyContent: 'center',
  },
  inputWrapper: {
    width: '100%',
    height: 70,
  },
  titleContainer: {
    alignItems: 'flex-start',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 25,
  },
  loginBtn: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    backgroundColor: '#a173e2',
    borderRadius: 10,
    width: '100%',
    height: 50,
  },
  loginBtnTitle: {
    fontWeight: 'bold',
    fontSize: 15,
    color: 'white',
  },
  errorMessage: {
    fontSize: 10,
    color: 'red',
  },
});

export default Login;
