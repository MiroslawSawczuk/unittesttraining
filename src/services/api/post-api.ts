export interface Post {
  id: number;
  userId: number;
  title: string;
  body: string;
}
export const fetchPost = async (id: number): Promise<Post> => {
  let data = {} as Post;

  try {
    const response = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${id}`,
    );
    data = await response.json();
  } catch (error) {
    console.log(error);
  }

  return data;
};
