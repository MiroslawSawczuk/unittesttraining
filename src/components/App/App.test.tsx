import {fireEvent, render, screen} from '@testing-library/react-native';
import React from 'react';
import App from './App';

const setup = () => {
  return render(<App />);
};

describe('App component', () => {
  it('renders without crashing', async () => {
    const component = setup().toJSON();
    expect(component).toMatchSnapshot();
  });

  it('After btn press, values should be visible', async () => {
    render(<App />);

    // global.fetch = jest.fn().mockImplementationOnce(() => {
    //   return new Promise((resolve, reject) => {
    //     resolve({
    //       ok: true,
    //       status: 200,
    //       json: () => ({
    //         id: 1,
    //         userId: 'MIREK',
    //         title: 'some title',
    //         body: 'some body',
    //       }),
    //     });
    //   });
    // });

    global.fetch = jest.fn().mockImplementationOnce(() =>
      Promise.resolve({
        ok: true,
        status: 200,
        json: () => ({
          id: 1,
          userId: 1,
          title: 'some title',
          body: 'some body',
        }),
      }),
    );

    fireEvent.press(screen.getByTestId('btn'));
    const userId = (await screen.findByTestId('userId')).props.children;

    expect(userId).toEqual(1);
  });
});
