import React, {useState} from 'react';
import {SafeAreaView, Text, TouchableOpacity} from 'react-native';
import {fetchPost, Post} from '../../services/api/post-api';

const App = () => {
  const [post, setPost] = useState<Post>();

  const onPress = async () => {
    const responsePost = await fetchPost(1);
    setPost(responsePost);
  };
  return (
    <SafeAreaView>
      <TouchableOpacity testID={'btn'} onPress={() => onPress()}>
        <Text>Fetch post</Text>
      </TouchableOpacity>
      <Text testID={'userId'}>{post?.userId}</Text>
      <Text>{post?.title}</Text>
      <Text>{post?.body}</Text>
    </SafeAreaView>
  );
};

export default App;
